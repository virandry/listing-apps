import React from 'react';
import './logo.scss';
import { Link } from 'react-router-dom';

function Logo() {
  return (
    <Link className="logo" to="/">
      <span className="logo__part-1">easy</span>
      <span className="logo__part-2">.my</span>
    </Link>
  );
}

export default Logo;
