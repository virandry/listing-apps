import HttpService from './HttpService';

export class EasyService extends HttpService {
  getList() {
    return this.get('/list').then(response => response.data);
  }

  getItem(id) {
    return this.get(`/view/${id}`).then(({ data }) => {
      return {
        id,
        type: data.data.type,
        attributes: { ...data.data.attributes, links: data.links },
      };
    });
  }

  getSimilarItems(id) {
    return this.get(`/similar/${id}`).then(response => response.data);
  }
}
