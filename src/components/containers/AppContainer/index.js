import PropTypes from 'prop-types';
import React from 'react';
import './app-container.scss';

function AppContainer(props) {
  const { children } = props;
  return (
    <div id="app-container" className="app-container">
      {children}
    </div>
  );
}

export default AppContainer;

AppContainer.propTypes = {
  children: PropTypes.node,
};
