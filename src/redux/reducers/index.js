import { combineReducers } from 'redux';
import listingReducer from './listingReducer';

const rootReducer = combineReducers({
  listingReducer,
});

export default rootReducer;
