import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function Price(props) {
  const { value, className } = props;
  return <div className={cx('card__price', className)}>{value}</div>;
}

export default Price;

Price.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string,
};
