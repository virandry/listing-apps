import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Paper from '../../components/containers/Paper';
import './view.scss';
import { easyService } from '../../services';
import Breadcrumbs from './components/Breadcrumbs';
import ItemImage from './components/ItemImage';
import ContactDetails from './components/ContactDetails';
import ContactButtons from './components/ContactButtons';
import ItemDescription from './components/ItemDescription';
import SimilarItems from './components/SimilarItems';

function View() {
  const { id } = useParams();
  const storedItemAttributes = useSelector(
    ({ listingReducer }) =>
      listingReducer.items?.find(item => item.id === id)?.attributes,
  );

  const [itemAttributes, setItemAttributes] = useState(storedItemAttributes);

  const fetchItemAttributes = useCallback(async () => {
    try {
      const response = await easyService.getItem(id);
      console.log(response);
      setItemAttributes(response.attributes);
    } catch (e) {
      console.log(e);
    }
  }, [id]);

  useEffect(() => {
    if (!itemAttributes) fetchItemAttributes();
  }, [itemAttributes, fetchItemAttributes]);

  useEffect(() => window.scrollTo(0, 0), []);

  return (
    <div id="main" className="view">
      <Paper className="view__paper">
        <Breadcrumbs id={id} title={itemAttributes?.title} />
        <div className="view__row">
          <div className="view__col">
            <div className="view__item-title">{itemAttributes?.title}</div>
          </div>
          <div className="view__col view__col--extra" />
          <div className="view__col">
            <ItemImage
              src={itemAttributes?.links?.image}
              alt={`Image of ${itemAttributes?.title}`}
            />
          </div>
          <div className="view__col view__col--extra">
            <ContactDetails
              price={itemAttributes?.price}
              condition={itemAttributes?.condition}
              location={itemAttributes?.location}
              sellerName={itemAttributes?.sellerName}
              sellerType={itemAttributes?.sellerType}
            />
            <ContactButtons phone={itemAttributes?.phone} />
          </div>
          <div className="view__col">
            <ItemDescription description={itemAttributes?.description} />
          </div>
          <div className="view__col view__col--extra" />
          <SimilarItems id={id} />
        </div>
      </Paper>
    </div>
  );
}

export default View;
