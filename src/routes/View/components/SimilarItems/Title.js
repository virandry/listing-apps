import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function Title(props) {
  const { className, children } = props;
  return <div className={cx('similar-card__title', className)}>{children}</div>;
}

export default Title;

Title.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
