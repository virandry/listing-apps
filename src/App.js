/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Listing from './routes/Listing';
import './styles/app.scss';
import View from './routes/View';
import AppContainer from './components/containers/AppContainer';
import Topbar from './components/Topbar';

export default function App() {
  return (
    <AppContainer>
      <Topbar />
      <Switch>
        <Route
          path="/view/:id"
          render={props => <View key={props.match.params.id} />}
        />
        <Route path="/" exact>
          <Listing />
        </Route>
      </Switch>
    </AppContainer>
  );
}
