import axios from 'axios';
import humps from 'humps';

class HttpService {
  axios;

  constructor(baseURL) {
    this.axios = axios.create({
      baseURL,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    this.reponseInterceptor();
  }

  reponseInterceptor() {
    this.axios.interceptors.response.use(
      response => humps.camelizeKeys(response),
      error => Promise.reject(error.response || error.message),
    );
  }

  get(url, params, config = {}) {
    return this.axios.get(url, {
      params,
      ...config,
    });
  }

  post(url, data, config = {}) {
    return this.axios.post(url, data, {
      ...config,
    });
  }

  put(url, data, config = {}) {
    return this.axios.put(url, data, {
      ...config,
    });
  }

  patch(url, data, config = {}) {
    return this.axios.patch(url, data, {
      ...config,
    });
  }

  delete(url, params, config = {}) {
    return this.axios.delete(url, {
      params,
      ...config,
    });
  }
}

export default HttpService;
