import { EasyService } from './EasyService';

const easyService = new EasyService(
  'https://5b35ede16005b00014c5dc86.mockapi.io/',
);

export { easyService };
