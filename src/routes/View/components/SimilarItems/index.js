import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import cx from 'classnames';
import { useHistory } from 'react-router-dom';
import { easyService } from '../../../../services';
import Card from './Card';
import './similar-items.scss';

const similarItemsInit = [];

function SimilarItems(props) {
  const { id, className } = props;
  const history = useHistory();

  const [similarItems, setSimilarItems] = useState(similarItemsInit);

  const fetchList = useCallback(async () => {
    try {
      const response = await easyService.getSimilarItems(id);
      console.log('I: ', response.data);
      setSimilarItems(response.data);
    } catch (e) {
      console.log(e);
    }
  }, [id]);

  useEffect(() => {
    fetchList();
  }, [fetchList]);

  const handleClick = useCallback(
    keyId => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
      history.push(`/view/${keyId}`);
    },
    [history],
  );

  return (
    <div className={cx('similar', className)}>
      <div className="similar__title">SIMILAR ITEMS</div>
      {similarItems.map(({ id: keyId, attributes }) => (
        <div key={keyId} className="similar__item-container">
          <Card onClick={() => handleClick(keyId)}>
            <Card.Thumbnail src={attributes?.image} />
            <Card.Title>{attributes?.title ?? ''}</Card.Title>
            <Card.Price value={attributes?.price ?? 0} />
          </Card>
        </div>
      ))}
    </div>
  );
}

export default SimilarItems;

SimilarItems.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
};
