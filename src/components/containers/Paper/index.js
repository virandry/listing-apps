import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import './paper.scss';

function Paper(props) {
  const { className, containerClassName, children, title } = props;
  return (
    <main className={cx('paper', className)}>
      <div className={cx('paper__container', containerClassName)}>
        {!!title && <div className="paper__title">{title}</div>}
        <div className="paper__content">{children}</div>
      </div>
    </main>
  );
}

export default Paper;

Paper.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  containerClassName: PropTypes.string,
  title: PropTypes.string,
};
