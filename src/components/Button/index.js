import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import cx from 'classnames';
import './button.scss';

function Button(props) {
  const {
    type = 'button',
    className,
    href,
    children,
    fullWidth = false,
    inverted,
    dull,
    flat,
    icon,
    ...rest
  } = props;
  const btnProps = useMemo(() => {
    return {
      ...rest,
      type,
      className: cx('btn', className, {
        btn__fullwidth: fullWidth,
        btn__inverted: inverted,
        btn__dull: dull,
        btn__flat: flat,
      }),
    };
  }, [flat, dull, fullWidth, className, rest, type, inverted]);
  if (href)
    return (
      <a href={href} {...btnProps}>
        {icon && <span className="btn__icon">{icon}</span>}
        {children}
      </a>
    );
  return (
    <button {...btnProps}>
      {icon && <span className="btn__icon">{icon}</span>}
      {children}
    </button>
  );
}

export default Button;

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  dull: PropTypes.bool,
  flat: PropTypes.bool,
  fullWidth: PropTypes.bool,
  href: PropTypes.string,
  icon: PropTypes.node,
  inverted: PropTypes.bool,
  type: PropTypes.string,
};

Button.defaultProps = {
  fullWidth: false,
  type: 'button',
};
