import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Thumbnail from './Thumbnail';
import Title from './Title';
import Price from './Price';

function Card(props) {
  const { className, children, onClick } = props;
  return (
    <div
      className={cx('similar-card', className, {
        'similar-card__cursor--pointer': !!onClick,
      })}
      onClick={onClick}
    >
      {children}
    </div>
  );
}
Card.Thumbnail = Thumbnail;
Card.Title = Title;
Card.Price = Price;
export default Card;

Card.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func,
};
