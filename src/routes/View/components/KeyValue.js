import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function KeyValue(props) {
  const { className, contentClassName, name, children } = props;

  return (
    <div className={cx('keyvalue', className)}>
      <div className="keyvalue__name">{name}</div>
      <div className={cx('keyvalue__content', contentClassName)}>
        {children}
      </div>
    </div>
  );
}

export default KeyValue;

KeyValue.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
  contentClassName: PropTypes.string,
  name: PropTypes.string,
};
