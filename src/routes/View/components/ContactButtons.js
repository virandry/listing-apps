import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faComments } from '@fortawesome/free-solid-svg-icons/faComments';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons/faAddressBook';
import Button from '../../../components/Button';

function ContactButtons(props) {
  const { className, phone } = props;
  return (
    <div className={cx('view__contact-buttons', className)}>
      <div className="view__legends">
        Interested with the ad? Contact the seller
      </div>
      <div className="view__row">
        <div className="view__contact-buttons--btn view__contact-buttons--btn-phone">
          <Button
            fullWidth
            inverted
            icon={<FontAwesomeIcon icon={faAddressBook} />}
          >
            {phone}
          </Button>
        </div>
        <div className="view__contact-buttons--btn">
          <Button
            fullWidth
            inverted
            icon={<FontAwesomeIcon icon={faEnvelope} />}
          >
            Email
          </Button>
        </div>
        <div className="view__contact-buttons--btn">
          <Button fullWidth icon={<FontAwesomeIcon icon={faComments} />}>
            Chat
          </Button>
        </div>
      </div>
    </div>
  );
}

export default ContactButtons;

ContactButtons.propTypes = {
  className: PropTypes.string,
  phone: PropTypes.string,
};
