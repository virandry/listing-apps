import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';

function Breadcrumbs(props) {
  const { id, className, title } = props;
  return (
    <div className={cx('view__breadcrumbs', className)}>
      Home &gt; Electronics &gt; Games & Console &gt;{' '}
      <Link className="view__breadcrumbs-link" to={`/view/${id}`}>
        {title}
      </Link>
    </div>
  );
}

export default Breadcrumbs;

Breadcrumbs.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  title: PropTypes.string,
};
