import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function Row(props) {
  const { className, children } = props;
  return <div className={cx('row', className)}>{children}</div>;
}

export default Row;

Row.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
