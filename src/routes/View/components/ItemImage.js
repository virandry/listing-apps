import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function ItemImage(props) {
  const { className, src, alt } = props;
  return (
    <div className={cx('view__item-image', className)}>
      <img className="view__image" src={src} alt={alt} />
    </div>
  );
}

export default ItemImage;

ItemImage.propTypes = {
  alt: PropTypes.string,
  className: PropTypes.string,
  src: PropTypes.string,
};
