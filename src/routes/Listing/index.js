import React, { useCallback, useEffect } from 'react';
import './listing.scss';
import './components/grid.scss';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { easyService } from '../../services';
import Paper from '../../components/containers/Paper';
import Card from '../../components/Card';
import Col from './components/Col';
import Row from './components/Row';
import listingActions from '../../redux/actions/listingActions';

function Listing() {
  const items = useSelector(({ listingReducer }) => listingReducer.items);
  const dispatch = useDispatch();
  const history = useHistory();

  const fetchList = useCallback(async () => {
    try {
      const response = await easyService.getList();
      dispatch(listingActions.setListing(response.data));
    } catch (e) {
      console.log(e);
    }
  }, [dispatch]);

  useEffect(() => {
    fetchList();
  }, [fetchList]);

  useEffect(() => window.scrollTo(0, 0), []);

  return (
    <div className="listing">
      <Paper title="LISTING" containerClassName="listing__paper-container">
        <Row>
          {items?.map(({ id, attributes }) => {
            return (
              <Col key={id}>
                <Card onClick={() => history.push(`/view/${id}`)}>
                  <Card.Thumbnail src={attributes?.links?.image} />
                  <Card.Title>{attributes?.title ?? ''}</Card.Title>
                  <Card.Price value={attributes?.price ?? 0} />
                </Card>
              </Col>
            );
          })}
        </Row>
      </Paper>
    </div>
  );
}

export default Listing;
