import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function Thumbnail(props) {
  const { className, src, alt } = props;
  return (
    <div className={cx('similar-card__thumbnail', className)}>
      <img className="similar-card__image" src={src} alt={alt} />
    </div>
  );
}

export default Thumbnail;

Thumbnail.propTypes = {
  alt: PropTypes.string,
  className: PropTypes.string,
  src: PropTypes.string,
};
