1. Clone the project
    ```shell script
    git clone git@gitlab.com:virandry/listing-apps.git listing-apps
    ```
2. Go to **grs** directory
    ```shell script
    cd listing-apps
    ```
3. Install dependencies and build the project
    ```shell script
    yarn install
    yarn build
    ```
4. Run `yarn start:prod`
5. Open browser and go to **http://localhost:3000**
