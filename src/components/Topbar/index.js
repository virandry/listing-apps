import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import './topbar.scss';
import { slide as Menu } from 'react-burger-menu';
import Logo from '../Logo';
import Button from '../Button';

function Topbar(props) {
  const { className, ...rest } = props;
  return (
    <nav className={cx('topbar', className)} {...rest}>
      <div className="topbar__container">
        <Logo />
        <ul className="topbar__list">
          <li className="topbar__item">
            <a className="topbar__link">Categories</a>
          </li>
          <li className="topbar__item">
            <a className="topbar__link">Notification</a>
          </li>
          <li className="topbar__item">
            <a className="topbar__link">Login /Sign up</a>
          </li>
          <li className="topbar__item">
            <a className="topbar__link">Help</a>
          </li>
        </ul>
        <Menu right>
          <Button flat inverted href="/">
            Categories
          </Button>
          <Button flat inverted href="/">
            Notification
          </Button>
          <Button flat inverted href="/">
            Login /Sign up
          </Button>
          <Button flat inverted href="/">
            Help
          </Button>
        </Menu>
      </div>
    </nav>
  );
}

export default Topbar;

Topbar.propTypes = {
  className: PropTypes.string,
};
