import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import cx from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFlag } from '@fortawesome/free-solid-svg-icons/faFlag';
import Button from '../../../components/Button';

function ItemDescription(props) {
  const { className, description } = props;
  const parsedDescription = useMemo(() => {
    return description?.split('\\n').map((item, index) => {
      return (
        <span key={index}>
          {item}
          <br />
        </span>
      );
    });
  }, [description]);
  return (
    <div className={cx('view__item-description', className)}>
      <div className="view__item-description--heading">
        <div className="view__item-description--title">DESCRIPTION</div>
        <div className="view__item-description--report">
          <Button dull flat icon={<FontAwesomeIcon icon={faFlag} />}>
            Report Ad
          </Button>
        </div>
      </div>
      <div className="view__item-description--content">{parsedDescription}</div>
    </div>
  );
}

export default ItemDescription;

ItemDescription.propTypes = {
  className: PropTypes.string,
  description: PropTypes.string,
};
