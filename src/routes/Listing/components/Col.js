import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

function Col(props) {
  const { className, children } = props;
  return <div className={cx('col', className)}>{children}</div>;
}

export default Col;

Col.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
