import { listingActions } from '../actionTypes';

const listingReducer = (state = { items: [] }, action) => {
  switch (action.type) {
    case listingActions.SET_LISTING:
      return {
        ...state,
        items: action.payload,
      };
    case listingActions.CLEAR_LISTING:
      return {
        ...state,
        items: [],
      };
    default:
      return state;
  }
};

export default listingReducer;
