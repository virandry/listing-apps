import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-regular-svg-icons/faHeart';
import { faShareSquare } from '@fortawesome/free-solid-svg-icons/faShareSquare';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons/faUserCircle';
import KeyValue from './KeyValue';
import Button from '../../../components/Button';

function ContactDetails(props) {
  const {
    className,
    price,
    condition,
    location,
    sellerName,
    sellerType,
  } = props;
  return (
    <div className={cx('view__contact-details', className)}>
      <div className="view__row">
        <div className="view__col view__col50">
          <Button dull flat icon={<FontAwesomeIcon icon={faHeart} />}>
            Wishlist
          </Button>
        </div>
        <div className="view__col view__col50">
          <Button dull flat icon={<FontAwesomeIcon icon={faShareSquare} />}>
            Share
          </Button>
        </div>
      </div>
      <KeyValue name="Price">
        <div className="keyvalue__content--price">{price}</div>
      </KeyValue>
      <KeyValue name="Item condition">{condition}</KeyValue>
      <KeyValue name="Item location">{location}</KeyValue>
      <KeyValue name="Seller info" contentClassName="keyvalue__seller">
        <div className="keyvalue__seller--avatar">
          <FontAwesomeIcon icon={faUserCircle} />
        </div>
        <div className="keyvalue__seller--info">
          <div className="keyvalue__seller--name">{sellerName}</div>
          <div className="keyvalue__seller--type">{sellerType}</div>
        </div>
      </KeyValue>
    </div>
  );
}

export default ContactDetails;

ContactDetails.propTypes = {
  className: PropTypes.string,
  condition: PropTypes.string,
  location: PropTypes.string,
  price: PropTypes.string,
  sellerName: PropTypes.string,
  sellerType: PropTypes.string,
};
