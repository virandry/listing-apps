import { listingActions } from '../actionTypes';

const setListing = items => {
  return {
    type: listingActions.SET_LISTING,
    payload: items,
  };
};

const clearListing = () => {
  return {
    type: listingActions.CLEAR_LISTING,
  };
};

export default {
  setListing,
  clearListing,
};
